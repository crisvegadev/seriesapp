import 'dart:convert';

import 'package:the_series_db/data/models/chracter_model.dart';
import 'package:the_series_db/data/models/episode_model.dart';
import 'package:the_series_db/data/models/image_model.dart';
import 'package:the_series_db/data/models/publisher_model.dart';
import 'package:the_series_db/domain/entities/character.dart';
import 'package:the_series_db/domain/entities/episode.dart';
import 'package:the_series_db/domain/entities/serie_entity.dart';

SerieModel serieFromJson(String str) => SerieModel.fromJson(json.decode(str));

String serieToJson(SerieModel data) => json.encode(data.toJson());

List<SerieModel> serieFromJsonToList(String str) =>
    SerieModel.jsonToList(json.decode(str));

class SerieModel extends SerieEntity {
  SerieModel({
    this.characters,
    this.episodes,
    this.publisher,
    required this.aliases,
    required this.apiDetailUrl,
    required this.countOfEpisodes,
    required this.dateAdded,
    required this.dateLastUpdated,
    required this.deck,
    required this.description,
    required this.firstEpisode,
    required this.id,
    required this.image,
    required this.lastEpisode,
    required this.name,
    required this.siteDetailUrl,
    required this.startYear,
  }) : super(
          characters: characters,
          episodes: episodes,
          aliases: aliases,
          apiDetailUrl: apiDetailUrl,
          countOfEpisodes: countOfEpisodes,
          dateAdded: dateAdded,
          dateLastUpdated: dateLastUpdated,
          deck: deck,
          description: description,
          firstEpisode: firstEpisode,
          id: id,
          image: image,
          lastEpisode: lastEpisode,
          name: name,
          publisher: publisher,
          siteDetailUrl: siteDetailUrl,
          startYear: startYear,
        );

  final dynamic aliases;
  final String apiDetailUrl;
  final List<CharacterModel>? characters;
  final int countOfEpisodes;
  final DateTime dateAdded;
  final DateTime dateLastUpdated;
  final dynamic deck;
  final String description;
  final List<EpisodeModel>? episodes;
  final EpisodeModel firstEpisode;
  final int id;
  final ImageModel image;
  final EpisodeModel lastEpisode;
  final String name;
  final PublisherModel? publisher;
  final String siteDetailUrl;
  final String startYear;

  static jsonToList(Map<String, dynamic> json) =>
      (json['results'] as List).map((i) => SerieModel.fromJson(i)).toList();

  factory SerieModel.fromJson(Map<String, dynamic> json) {
    json = (json["results"]) ?? json; // Validación de Null - safety
    return SerieModel(
      aliases: json["aliases"],
      apiDetailUrl: json["api_detail_url"],
      characters: (json["characters"] != null)
          ? List<CharacterModel>.from(
              json["characters"].map((x) => CharacterModel.fromJson(x)))
          : null,
      countOfEpisodes: json["count_of_episodes"],
      dateAdded: DateTime.parse(json["date_added"]),
      dateLastUpdated: DateTime.parse(json["date_last_updated"]),
      deck: json["deck"],
      description: json["description"] ?? 'no description',
      episodes: (json["episodes"] != null)
          ? List<EpisodeModel>.from(
              json["episodes"].map((x) => EpisodeModel.fromJson(x)))
          : null,
      firstEpisode: EpisodeModel.fromJson(json["first_episode"]),
      id: json["id"],
      image: ImageModel.fromJson(json["image"]),
      lastEpisode: EpisodeModel.fromJson(json["last_episode"]),
      name: json["name"],
      publisher: (json["publisher"] != null)
          ? PublisherModel.fromJson(json["publisher"])
          : null,
      siteDetailUrl: json["site_detail_url"],
      startYear: json["start_year"],
    );
  }

  Map<String, dynamic> toJson() => {
        "aliases": aliases,
        "api_detail_url": apiDetailUrl,
        "characters": List<Character>.from(characters!.map((x) => x.toJson())),
        "count_of_episodes": countOfEpisodes,
        "date_added": dateAdded.toIso8601String(),
        "date_last_updated": dateLastUpdated.toIso8601String(),
        "deck": deck,
        "description": description,
        "episodes": List<Episode>.from(episodes!.map((x) => x.toJson())),
        "first_episode": firstEpisode.toJson(),
        "id": id,
        "image": image.toJson(),
        "last_episode": lastEpisode.toJson(),
        "name": name,
        "publisher": publisher!.toJson(),
        "site_detail_url": siteDetailUrl,
        "start_year": startYear,
      };
}
