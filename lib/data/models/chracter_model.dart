import 'package:the_series_db/domain/entities/character.dart';

class CharacterModel extends Character {
  CharacterModel({
    required this.apiDetailUrl,
    required this.id,
    required this.name,
    required this.count,
    this.siteDetailUrl,
  }) : super(
          apiDetailUrl: apiDetailUrl,
          id: id,
          name: name,
          count: count,
          siteDetailUrl: siteDetailUrl,
        );

  final String apiDetailUrl;
  final int id;
  final String name;
  final String? siteDetailUrl;
  final String count;

  factory CharacterModel.fromJson(Map<String, dynamic> json) => CharacterModel(
        apiDetailUrl: json["api_detail_url"],
        id: json["id"],
        name: json["name"],
        siteDetailUrl: json["site_detail_url"],
        count: json["count"],
      );

  Map<String, dynamic> toJson() => {
        "api_detail_url": apiDetailUrl,
        "id": id,
        "name": name,
        "site_detail_url": siteDetailUrl,
        "count": count,
      };
}
