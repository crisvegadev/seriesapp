import 'package:the_series_db/domain/entities/episode.dart';

class EpisodeModel extends Episode {
  EpisodeModel({
    required this.apiDetailUrl,
    required this.id,
    required this.name,
    this.siteDetailUrl,
    required this.episodeNumber,
  }) : super(
          apiDetailUrl: apiDetailUrl,
          id: id,
          name: name,
          siteDetailUrl: siteDetailUrl,
          episodeNumber: episodeNumber,
        );

  final String apiDetailUrl;
  final int id;
  final String name;
  final String? siteDetailUrl;
  final String episodeNumber;

  Map<String, dynamic> toJson() {
    return {
      'api_detail_url': apiDetailUrl,
      'id': id,
      'name': name,
      'site_detail_url': siteDetailUrl,
      'episode_number': episodeNumber,
    };
  }

  factory EpisodeModel.fromJson(Map<String, dynamic> json) {
    return EpisodeModel(
      apiDetailUrl: json['api_detail_url'],
      id: json['id'],
      name: json['name'],
      siteDetailUrl: json['site_detail_url'],
      episodeNumber: json['episode_number'],
    );
  }
}
