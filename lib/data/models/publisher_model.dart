import 'package:the_series_db/domain/entities/publisher.dart';

class PublisherModel extends Publisher {
  PublisherModel({
    required this.apiDetailUrl,
    required this.id,
    required this.name,
  }) : super(
          apiDetailUrl: apiDetailUrl,
          id: id,
          name: name,
        );

  final String apiDetailUrl;
  final int id;
  final String name;

  Map<String, dynamic> toJson() {
    return {
      'api_detail_url': apiDetailUrl,
      'id': id,
      'name': name,
    };
  }

  factory PublisherModel.fromJson(Map<String, dynamic> json) {
    return PublisherModel(
      apiDetailUrl: json['api_detail_url'],
      id: json['id'],
      name: json['name'],
    );
  }
}
