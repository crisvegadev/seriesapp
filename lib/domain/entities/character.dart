import 'package:equatable/equatable.dart';

class Character extends Equatable {
  Character({
    required this.apiDetailUrl,
    required this.id,
    required this.name,
    required this.count,
    this.siteDetailUrl,
  });

  final String apiDetailUrl;
  final int id;
  final String name;
  final String? siteDetailUrl;
  final String count;

  @override
  List<Object?> get props {
    return [
      apiDetailUrl,
      id,
      name,
      siteDetailUrl,
      count,
    ];
  }
}
