import 'package:equatable/equatable.dart';

class Episode extends Equatable {
  Episode({
    required this.apiDetailUrl,
    required this.id,
    required this.name,
    this.siteDetailUrl,
    required this.episodeNumber,
  });

  final String apiDetailUrl;
  final int id;
  final String name;
  final String? siteDetailUrl;
  final String episodeNumber;

  @override
  List<Object?> get props {
    return [
      apiDetailUrl,
      id,
      name,
      siteDetailUrl,
      episodeNumber,
    ];
  }
}
