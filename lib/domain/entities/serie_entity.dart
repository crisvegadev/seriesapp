import 'package:equatable/equatable.dart';
import 'package:the_series_db/domain/entities/character.dart';
import 'package:the_series_db/domain/entities/episode.dart';
import 'package:the_series_db/domain/entities/image_serie.dart';
import 'package:the_series_db/domain/entities/publisher.dart';

class SerieEntity extends Equatable {
  SerieEntity({
    required this.aliases,
    required this.apiDetailUrl,
    this.characters,
    required this.countOfEpisodes,
    required this.dateAdded,
    required this.dateLastUpdated,
    required this.deck,
    required this.description,
    this.episodes,
    required this.firstEpisode,
    required this.id,
    required this.image,
    required this.lastEpisode,
    required this.name,
    this.publisher,
    required this.siteDetailUrl,
    required this.startYear,
  });

  final dynamic aliases;
  final String apiDetailUrl;
  final List<Character>? characters;
  final int countOfEpisodes;
  final DateTime dateAdded;
  final DateTime dateLastUpdated;
  final dynamic deck;
  final String description;
  final List<Episode>? episodes;
  final Episode firstEpisode;
  final int id;
  final ImageSerie image;
  final Episode lastEpisode;
  final String name;
  final Publisher? publisher;
  final String siteDetailUrl;
  final String startYear;

  @override
  List<Object?> get props => [
        aliases,
        apiDetailUrl,
        characters,
        countOfEpisodes,
        dateAdded,
        dateLastUpdated,
        deck,
        description,
        episodes,
        firstEpisode,
        id,
        image,
        lastEpisode,
        name,
        publisher,
        siteDetailUrl,
        startYear,
      ];
}
