import 'package:equatable/equatable.dart';

class Publisher extends Equatable {
  Publisher({
    required this.apiDetailUrl,
    required this.id,
    required this.name,
  });

  final String apiDetailUrl;
  final int id;
  final String name;

  @override
  List<Object?> get props => [apiDetailUrl, id, name];
}
