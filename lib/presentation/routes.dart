import 'package:get/get.dart';
import 'package:the_series_db/presentation/getx/list_series/list_series_binding.dart';
import 'package:the_series_db/presentation/pages/details/details_page.dart';
import 'package:the_series_db/presentation/pages/home/home_page.dart';

import 'getx/details/details_serie_binding.dart';

List<GetPage<dynamic>>? routes() => [
      GetPage(
        name: '/',
        page: () => HomePage(),
        binding: ListSeriesBinding(),
      ),
      GetPage(
        name: '/details',
        page: () => DetailsSeriePage(),
        binding: DetailsSerieBending(),
      ),
    ];
