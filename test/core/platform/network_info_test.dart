import 'package:flutter_test/flutter_test.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:the_series_db/core/platform/network_info.dart';

import 'network_info_test.mocks.dart';

@GenerateMocks([InternetConnectionChecker])
void main() {
  NetworkInfoImpl? networkInfoImpl;
  MockInternetConnectionChecker? mockInternetConnectionChecker;

  setUp(() {
    mockInternetConnectionChecker = MockInternetConnectionChecker();
    networkInfoImpl =
        NetworkInfoImpl(dataConnectionChecker: mockInternetConnectionChecker!);
  });

  group('isConnected', () {
    test('Should forward the call to DataConnectionChecker.hasConnection',
        () async {
      //arrange
      final tHasConnectionFuture = true;

      when(mockInternetConnectionChecker!.hasConnection)
          .thenAnswer((_) async => tHasConnectionFuture);

      //act
      final result = await networkInfoImpl!.isConnected;

      //assert
      verify(mockInternetConnectionChecker!.hasConnection);

      expect(result, tHasConnectionFuture);
    });
  });
}
